name = "gdpr-rights"

from .registry import ControllerRegistry
from .data_controller import DataController
from .data_subject import DataSubject