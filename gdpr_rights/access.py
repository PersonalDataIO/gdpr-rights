from jinja2 import Environment

def access_message(name, controller, requested_data=None, authentication_data=None, comment=None):
    from .template import access as access_text
    jinja_env = Environment(trim_blocks=True)
    access_template = jinja_env.from_string(access_text)
    return access_template.render(
        name=name,
        controller=controller,
        requested_data=requested_data,
        authentication_data=authentication_data,
        comment=comment
    )
