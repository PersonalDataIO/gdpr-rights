class DataController(object):
    def __init__(self, name=None, email=None):
        self._name = name
        self._email = email

    def __str__(self):
        return f"Data controller: {self._name}, reachable at {self._email}"

    @property
    def name(self):
        return self._name

    @property
    def email(self):
        return self._email

    def access_message(self, name, requested_data=None, authentication_data=None, comment=None):
        from .access import access_message
        return access_message(name, self, requested_data=requested_data, authentication_data=authentication_data, comment=comment)
