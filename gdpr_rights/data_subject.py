class DataSubject(object):
    def __init__(self, name=None, email=None):
        self._name = name
        self._email = email

    def __str__(self):
        return f"Data subject: {self._name}, reachable at {self._email}"

    def name(self):
        return self._name

    def email(self):
        return self._email

    def access(self, controller, requested_data=None, authentication_data=None, comment=None):
        # barring some more reasonable alternative preserving data subject rights in full,
        # _suggested_ by the data controller, use email instead
        from .access import email_send
        email_send(self, sender_name, sender_email, requested_data=None, authentication_data=None, comment=None)