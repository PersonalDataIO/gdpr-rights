access = \
"""
Dear {{controller.name}},

My name is {{ name }}.

Under the General Data Protection Regulation's Article 15.3, individuals ("data subjects") have a right of access to 
their personal data (see https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32016R0679 ).

Through this letter, I wish to exercise this right, and get access to all of my personal data.
This request was composed with the assistance of PersonalData.IO's online tool. 

According to GDPR Article 4, personal data is to be understood extremely broadly as any information relating to an 
identified or identifiable individual.

Please bear in mind that pseudonimized data is considered personal data for the purposes of the GDPR, and that the 
concept of "Personally Identifiable Information", which might be more familiar to you, is not actually relevant to 
interpreting the GDPR provisions.

{% if requested_data %}
While I wish to get a copy of all my personal data, I particularly wish to get access to:
{% for data_point in requested_data %}
  - {{ data_point }}
{% endfor %}

{% endif %}
Please also provide information on:
  - recipients of this data (in accordance with Art. 15.1(c))
  - sources of this data (in accordance with Art. 15.1(g))

In accordance with Art 15.1(h), please also provide any information on the existence of any automated 
decision-making, including profiling (as defined in Article 22).

I also wish to know the logic of the processing of my personal data, including at the very least a definite list of
inputs for any decision-making affecting me.

When responding to this request, I encourage you to pay attention to the applicable deadline of 30 days.

{% if authentication_data %}
Please find below some information that might be useful in confirming my identity.
{% for user_attribute in authentication_data %}
  - {{ user_attribute.0 }} : {{ user_attribute.1 }} 
{% endfor %}

{% endif %}
{% if comment %}
{{ comment }}

{% endif %}
Do let me know if any information you might need is missing, and inform me clearly and explicitly when you consider 
you have provided all the required information. Please pay attention to the requirements in Article 11 in order to 
do so.

Sincerely,

                                                                            {{ name }}
  
"""
