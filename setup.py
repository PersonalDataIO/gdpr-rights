import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="gdpr-rights",
    version="0.0.1",
    author="Paul-Olivier Dehaye",
    author_email="paulolivier@personaldata.io",
    description="A package facilitating the exercise of GDPR data subject rights",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/PersonalDataIO/gdpr-rights/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)